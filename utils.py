from skhep.math import LorentzVector
from itertools import combinations
import numpy as np
from scipy import special

def mass(v):
    a1 = LorentzVector(0,0,1,1)
    a1.setptetaphim(*v[0][:3],0)
    a2 = LorentzVector(0,0,1,1)
    a2.setptetaphim(*v[1][:3],0)
    return (a1+a2).mass

def dimuon_mass(evt):
    hypothesis = np.array((evt['lep_pt'],evt['lep_eta'],evt['lep_phi'],evt['lep_type'],evt['lep_charge'],evt['lep_isTightID'])).T
    hypothesis = np.array([[hypothesis[i,0:3],hypothesis[j,0:3]] for (i,j) in combinations(np.arange(hypothesis.shape[0]),2) 
                           if (hypothesis[i,3] == 13 and hypothesis[j,3] == 13 
                               and hypothesis[i,4] == -hypothesis[j,4]
                               and hypothesis[i,5] and hypothesis[j,5])])
    hypothesis.reshape(-1,3)
    masses = np.array([mass(x) for x in hypothesis])
    return masses

def voigtian(x,A,sigma):
    gamma = 2495.2
    m = x - 91187.6
    z = m + gamma*(1j)
    z = z/(sigma*np.sqrt(2))
    w = special.wofz(z)
    return A*w.real/(sigma*np.sqrt(2*np.pi))

def voigtian_bkg(x,A,m0,sigma,p0,p1):
    gamma = 2495.2
    m = x - m0
    z = m + gamma*(1j)
    z = z/(sigma*np.sqrt(2))
    w = special.wofz(z)
    bkgd = p0 + p1*(96000-x)/1000
    sig = A*w.real/(sigma*np.sqrt(2*np.pi))
    return sig + bkgd